FROM python:3.12-bookworm AS websitefeeder

WORKDIR /usr/src/app
COPY website/feeder/requirements.txt ./
RUN pip install -r requirements.txt

COPY libsubotto libsubotto
RUN pip install -e libsubotto

COPY website/feeder ./

CMD python -m libsubotto.data && python website_feeder.py


FROM python:3.11-bookworm AS website24web

WORKDIR /usr/src/app
COPY website/24web/requirements.txt ./

RUN pip install -r requirements.txt

COPY libsubotto libsubotto
RUN pip install -e libsubotto

COPY website/24web ./

CMD ./24h.py


FROM nginx:1.25-alpine AS proxy

COPY website/24web/nginx/nginx.conf /etc/nginx/conf.d
COPY website/24web/nginx/nginx-variables /etc/nginx/templates/10-variables.conf.template
COPY website/24web/nginx/config-nginx.conf /etc/nginx/nginx.conf
